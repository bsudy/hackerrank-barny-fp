module Hackerrank.CrossWordsSpec where
  import SpecHelper
  import Hackerrank.CrossWords
  import Data.Vector as Vector
  import Data.List
  import Data.Maybe



  spec :: Spec
  spec = do
    describe "crosswords" $ do
--      it "should replace all fully fitting word" $
--        replaceWordIn (Vector.fromList ['-','-','-']) "foo" `shouldBe` Vector.fromList ['f','o','o']
--
--      it "should replace fitting word" $
--        replaceWordIn (Vector.fromList ['-','-','-','+']) "foo" `shouldBe` Vector.fromList ['f','o','o','+']


      it "should match the word" $
        matchA "---" "foo" `shouldBe` True

      it "should match the word" $
        matchA "---+" "foo" `shouldBe` True

      it "should match the word2" $
        matchA "----" "foo" `shouldBe` False

      it "should match the word2" $
        matchA "--+-" "foo" `shouldBe` False

      it "should match the word2" $
        matchA "--+---" "foo" `shouldBe` False

      it "should replace word" $
        (replaceWordInString "---" "foo") `shouldBe` ["foo"]

      it "should replace word2" $
        (replaceWordInString "---+" "foo") `shouldBe` ["foo+"]

      it "should replace word2" $
        replaceWordInString "----+" "foo" `shouldBe` []
      it "should replace word3" $
        (fromMaybe [] $ Prelude.sequence $ Data.List.filter filterMayBe $replaceWord "+---+---+" "foo") `shouldBe` ["+foo+---+","+---+foo+"]

      it "should replace in place" $
        replaceInPlace "1234" "foo" `shouldBe` "foo4"

      it "should replace in matrix" $
        replaceInMatrix [ "+---+", "+++++", "+++++" ] "foo" `shouldBe` [[ "+foo+", "+++++", "+++++" ]]

      it "should replace in matrix" $
        replaceInMatrix [ "+-o-+", "+++++", "+++++" ] "foo" `shouldBe` [[ "+foo+", "+++++", "+++++" ]]

      it "should replace in matrix" $
        replaceInMatrix [ "+++++", "+---+", "+++++" ] "foo" `shouldBe` [[ "+++++", "+foo+", "+++++" ]]

      it "should replace in matrix" $
        replaceInWholeMatrix [ "++-++", "+---+", "++-++" ] "foo" `shouldBe` [[ "++-++", "+foo+", "++-++" ],["++f++","+-o-+","++o++"]]

      it "should solve crosswords" $
        crosswords ["+-++++++++","+-++++++++","+-++++++++","+-----++++","+-+++-++++","+-+++-++++","+++++-++++","++------++","+++++-++++","+++++-++++"] ["LONDON","DELHI","ICELAND","ANKARA"] `shouldBe` ["+L++++++++","+O++++++++","+N++++++++","+DELHI++++","+O+++C++++","+N+++E++++","+++++L++++","++ANKARA++","+++++N++++","+++++D++++"]

  main :: IO ()
  main = hspec spec

