

module Main where

  import Data.List.Split
  import Hackerrank.CrossWords
  import Control.Monad

  gcd' :: Integral a => a -> a -> a
  gcd' n m | n == m = n
           | n > m = gcd' (n - m) m
           | n < m = gcd' m n
  -- This part is related to the Input/Output and can be used as it is
  -- Do not modify it
--  main = do
--    input <- getLine
--    print . uncurry gcd' . listToTuple . convertToInt . words $ input
--   where
--    listToTuple (x:xs:_) = (x,xs)
--    convertToInt = map (read :: String -> Int)


--
--  fib n | n == 1 = 0
--        | n == 2 = 1
--        | n > 2 = fib (n-1) + fib (n-2)

--  main = do
--    input <- getLine
--    print . fib . (read :: String -> Int) $ input

--  match :: (Vector Char, String) -> (Vector Char)
--
--  canMatch :: (Vector Char, String) -> boolean
--
--
--  crosswords101 :: (Matrix Char, String) -> Matrix Char
--  crosswords101 (matrix, word) =  [ x | x <- [0..(nrows matrix), row <- getRow x matrix, if canMatch row word  ]


  main = do
    matrix <- replicateM 10 getLine
    words <- getLine
    putStr $ unlines $ crosswords matrix (splitOn ";" words)