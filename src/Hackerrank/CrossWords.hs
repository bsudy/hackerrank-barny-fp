module Hackerrank.CrossWords where
--  import Data.Matrix as Matrix
--  import Data.Vector as Vector
  import Data.List as List
  import Data.Maybe

  fib n | n == 1 = 0
        | n == 2 = 1
        | n > 2 = fib (n-1) + fib (n-2)

--  replaceWordIn :: Vector Char -> String -> Vector Char
--  replaceWordIn v w = v

  mapper :: Char -> Maybe String -> Maybe String
  mapper ch maybe = fmap (ch :) maybe

  filterMayBe :: Maybe a -> Bool
  filterMayBe Nothing = False
  filterMayBe a       = True

  replaceWordInString :: String -> String -> [String]
  replaceWordInString v w = fromMaybe [] $ Prelude.sequence $ filter filterMayBe $ replaceWord v w

  replaceWord :: String -> String -> [Maybe String]
  replaceWord v w = replaceWordCh  '+' v w

  replaceWordCh :: Char -> String -> String -> [Maybe String]
  replaceWordCh _ [] _ = []
  replaceWordCh p v@(vh:vt) w | p == '-' = List.map (fmap (vh :)) (replaceWordCh vh vt w)
                              | otherwise =  (if (matchA v w) then Just (replaceInPlace v w) else Nothing) : List.map (fmap (vh :)) (replaceWordCh vh vt w)



  replaceInPlace :: String -> String -> String
  replaceInPlace [] _ = []
  replaceInPlace v [] = v
  replaceInPlace (vh:vt) (wh:wt) = wh : replaceInPlace vt wt

  matchA :: String -> String -> Bool
  matchA [] [] = True
  matchA (vh:vt) [] | vh == '+' = True
                    | otherwise = False
  matchA [] _ = False
  matchA (vh:vt) (wh:wt) = (matchChar vh wh) && (matchA vt wt)

  matchChar :: Char -> Char -> Bool
  matchChar vc wc | vc == '-' = True
                  | vc == wc = True
                  | otherwise = False

  replaceInMatrix :: [String] -> String -> [[String]]
  replaceInMatrix [] w = []
  replaceInMatrix (h:t) w = (map (:t)(replaceWordInString h w)) ++ (fmap (h :)(replaceInMatrix t w))

  replaceInWholeMatrix :: [String] -> String -> [[String]]
  replaceInWholeMatrix m w = replaceInMatrix m w ++ (map transpose (replaceInMatrix (transpose m) w))


  crosswords2 :: [[String]] -> [String] -> [[String]]
  crosswords2 ms [] = ms
  crosswords2 ms (w:ws) = crosswords2 (foldMap (\x -> replaceInWholeMatrix x w) ms) ws

  crosswords :: [String] -> [String] -> [String]
  crosswords m ws = head $ crosswords2 [m] ws

  head' :: [a] -> Maybe a
  head' []     = Nothing
  head' (x:xs) = Just x